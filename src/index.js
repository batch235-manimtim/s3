/*
	Mini-Activity: 15 mins

	Create an expressjs api running at port 4000.

	Make sure to add a message that the server is running.

	Install nodemon, set up your package.json with nodemon

	Install mocha, chai, chai-http

	Set up mocha in package.json

	Run your terminal and send it in our Batch Hangouts

*/

const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());

let users = [
	{ name: 'Jojo Joestar', age: 25, username: 'Jojo' },
	{ name: 'Dio Brando', age: 23, username: 'Dio' },
	{ name: 'Jotaro Kujo', age: 28, username: 'Jotaro' }

] ;

let artists = [
	{
		name: "Dream Theater",
		songs: ["The Dance of Eternity", "Regression"],
		album: "Metropolis Pt. 2: Scenes from a Memory",
		isActive: true,
	},
	{
		name: "DragonForce",
		songs: ["Through the fire and flames", "Body Breakdown	"],
		album: "Inhuman Rampage!",
		isActive: true,
	},
	{
		name: "Killswitch Engage",
		songs: ["Unleashed", "Take Control"],
		album: "Atonement",
		isActive: true,
	},
];


//[TESTING empty users to fail the test]
// let users;

// [SECTION] Routes and Controllers for USERS

app.get("/users", (req, res) => {
	console.log(users);
	return res.send(users);
});

app.post("/users", (req, res) => {

	// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)
	// hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object
	if(!req.body.hasOwnProperty("name")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}

	if(!req.body.hasOwnProperty("age")){
		return res.status(400).send({
			error: "Bad Request - missing required parameter AGE"
		})
	}
})

//-----------ACTIVITY
app.get("/artists", (req, res) => {
	console.log(artists);
	return res.send(artists);
});

app.post("/artists", (req, res) => {
	if (
		!req.body.hasOwnProperty("name") ||
		!req.body.hasOwnProperty("songs") ||
		!req.body.hasOwnProperty("album") ||
		req.body.isActive === false
	) {
	return res.status(400).send({
		error: "Bad Request - missing required parameter NAME, ALBUM or SONGS",
	});
	}
});




app.listen(PORT, () => console.log(`Running on port ${PORT}`));